from big_class import BigClass
import unittest
import unittest.mock


class ReverseName(unittest.TestCase):

    def test_reverse_name(self):
        with unittest.mock.patch.object(BigClass, "__init__", lambda x: None):
            bc = BigClass()
            testString = "abcdef"
            assert bc.reverseName(testString) == testString[::-1]
            
    def test_without_patch(self):
        bc = BigClass()
        testString = "abcdef"
        assert bc.reverseName(testString) == testString[::-1]


if __name__ == '__main__':
    unittest.main()
